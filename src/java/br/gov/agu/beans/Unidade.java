package br.gov.agu.beans;

public class Unidade {

    private String nome;
    private int id_orgao;
    private String created;
    private String modified;
    private boolean ativo;
    private int id;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getId_orgao() {
        return id_orgao;
    }

    public void setId_orgao(int id_orgao) {
        this.id_orgao = id_orgao;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
