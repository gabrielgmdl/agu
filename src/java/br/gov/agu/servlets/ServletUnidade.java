package br.gov.agu.servlets;

import br.gov.agu.beans.Unidade;
import br.gov.agu.dao.UnidadeDAO;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.jasper.JasperException;

@WebServlet("/Unidade")
public class ServletUnidade extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/acesso_restrito.jsp");
        disp.forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            req.setCharacterEncoding("UTF-8");
            resp.setContentType("text/html;charset=UTF-8");

            String botao = req.getParameter("botao");

            //BOTÃO DE REDIRECIONAR PARA AS PÁGINAS DE CADASTRAR || EDITAR || ATIVAR || DESATIVAR
            if (botao.equals("Cadastrar")) {
                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/cadastrarUnidade.jsp");
                disp.forward(req, resp);
            } else if (botao.equals("Editar")) {
                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/editarUnidade.jsp");
                disp.forward(req, resp);
            } else if (botao.equals("Ativar")) {
                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/ativarUnidade.jsp");
                disp.forward(req, resp);
            } else if (botao.equals("Desativar")) {
                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/desativarUnidade.jsp");
                disp.forward(req, resp);
            } 

            //BOTÃO DE CADASTRAR || EDITAR || ATIVAR || DESATIVAR
            
            else if (botao.equals("cadastrar")) {

                String unidade = req.getParameter("unidade");
                int id_orgao = Integer.valueOf(req.getParameter("id_orgao"));

                Unidade nova = new Unidade();

                nova.setNome(unidade);
                nova.setId_orgao(id_orgao);

                UnidadeDAO.inserir(nova);

                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/cadastrarUnidade.jsp");
                disp.forward(req, resp);

            } else if (botao.equals("editar")) {

                int id = Integer.valueOf(req.getParameter("id"));
                String unidade = req.getParameter("unidade");
                int id_orgao = Integer.valueOf(req.getParameter("id_orgao"));

                Unidade antiga = new Unidade();

                antiga.setId(id);
                antiga.setNome(unidade);
                antiga.setId_orgao(id_orgao);

                UnidadeDAO.editar(antiga);

                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/editarOrgao.jsp");
                disp.forward(req, resp);

            } else if (botao.equals("ativar")) {

                int id = Integer.valueOf(req.getParameter("id"));
                Unidade antiga = new Unidade();
                antiga.setId(id);
                UnidadeDAO.ativar(antiga);

                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/ativarUnidade.jsp");
                disp.forward(req, resp);

            } else if (botao.equals("desativar")) {

                int id = Integer.valueOf(req.getParameter("id"));
                Unidade antiga = new Unidade();
                antiga.setId(id);
                UnidadeDAO.desativar(antiga);

                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/desativarUnidade.jsp");
                disp.forward(req, resp);

            } else {
                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/erro_bd.jsp");
                disp.forward(req, resp);
            }

        } catch (SQLException sqlerro) {
            RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/erro_bd.jsp");
            disp.forward(req, resp);
        } catch (NullPointerException nullerro) {
            RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/erro_bd.jsp");
            disp.forward(req, resp);
        } catch (JasperException jsperro) {
            RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/erro_bd.jsp");
            disp.forward(req, resp);
        }

    }

}
