package br.gov.agu.servlets;

import br.gov.agu.beans.Setor;
import br.gov.agu.dao.SetorDAO;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.jasper.JasperException;

@WebServlet("/Setor")
public class ServletSetor extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/acesso_restrito.jsp");
        disp.forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            req.setCharacterEncoding("UTF-8");
            resp.setContentType("text/html;charset=UTF-8");

            String botao = req.getParameter("botao");

            if (botao.equals("cadastrar")) {

                String setor = req.getParameter("setor");
                int id_unidade = Integer.valueOf(req.getParameter("id_unidade"));

                Setor novo = new Setor();

                novo.setNome(setor);
                novo.setId_unidade(id_unidade);

                SetorDAO.inserir(novo);

                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/cadastrarSetor.jsp");
                disp.forward(req, resp);

            } else if (botao.equals("editar")) {

                int id = Integer.valueOf(req.getParameter("id"));
                String setor = req.getParameter("setor");
                int id_unidade = Integer.valueOf(req.getParameter("id_unidade"));

                Setor antigo = new Setor();

                antigo.setId(id);
                antigo.setNome(setor);
                antigo.setId_unidade(id_unidade);

                SetorDAO.editar(antigo);

                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/editarSetor.jsp");
                disp.forward(req, resp);

            } else if (botao.equals("ativar")) {

                int id = Integer.valueOf(req.getParameter("id"));
                Setor antigo = new Setor();
                antigo.setId(id);
                SetorDAO.ativar(antigo);

                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/ativarSetor.jsp");
                disp.forward(req, resp);

            } else if (botao.equals("desativar")) {

                int id = Integer.valueOf(req.getParameter("id"));
                Setor antigo = new Setor();
                antigo.setId(id);
                SetorDAO.desativar(antigo);

                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/desativarSetor.jsp");
                disp.forward(req, resp);

            } else {
                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/erro_bd.jsp");
                disp.forward(req, resp);
            }

        } catch (SQLException sqlerro) {
            RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/erro_bd.jsp");
            disp.forward(req, resp);
        } catch (NullPointerException nullerro) {
            RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/erro_bd.jsp");
            disp.forward(req, resp);
        } catch (JasperException jsperro) {
            RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/erro_bd.jsp");
            disp.forward(req, resp);
        }

    }

}
