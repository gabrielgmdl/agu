package br.gov.agu.servlets;

import br.gov.agu.beans.Orgao;
import br.gov.agu.dao.OrgaoDAO;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.jasper.JasperException;

@WebServlet("/Orgao")
public class ServletOrgao extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");

        RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/acesso_restrito.jsp");
        disp.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            req.setCharacterEncoding("UTF-8");
            resp.setContentType("text/html;charset=UTF-8");

            String botao = req.getParameter("botao");

            //BOTÃO DE REDIRECIONAR PARA AS PÁGINAS DE CADASTRAR || EDITAR || ATIVAR || DESATIVAR
            if (botao.equals("Cadastrar")) {
                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/cadastrarOrgao.jsp");
                disp.forward(req, resp);
            } else if (botao.equals("Editar")) {
                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/editarOrgao.jsp");
                disp.forward(req, resp);
            } else if (botao.equals("Ativar")) {
                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/ativarOrgao.jsp");
                disp.forward(req, resp);
            } else if (botao.equals("Desativar")) {
                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/desativarOrgao.jsp");
                disp.forward(req, resp);
            } //BOTÃO DE CADASTRAR || EDITAR || ATIVAR || DESATIVAR
            else if (botao.equals("cadastrar")) {

                String orgao = req.getParameter("orgao");
                String estado = req.getParameter("estado");
                String cidade = req.getParameter("cidade");

                Orgao novo = new Orgao();

                novo.setNome(orgao);
                novo.setEstado(estado);
                novo.setCidade(cidade);

                OrgaoDAO.inserir(novo);

                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/cadastrarOrgao.jsp");
                disp.forward(req, resp);

            } else if (botao.equals("editar")) {

                int id = Integer.valueOf(req.getParameter("id"));
                String orgao = req.getParameter("orgao");
                String estado = req.getParameter("estado");
                String cidade = req.getParameter("cidade");

                Orgao antigo = new Orgao();

                antigo.setId(id);
                antigo.setNome(orgao);
                antigo.setEstado(estado);
                antigo.setCidade(cidade);

                OrgaoDAO.editar(antigo);

                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/editarOrgao.jsp");
                disp.forward(req, resp);

            } else if (botao.equals("ativar")) {

                int id = Integer.valueOf(req.getParameter("id"));
                Orgao antigo = new Orgao();
                antigo.setId(id);
                OrgaoDAO.ativar(antigo);

                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/ativarOrgao.jsp");
                disp.forward(req, resp);

            } else if (botao.equals("desativar")) {

                int id = Integer.valueOf(req.getParameter("id"));
                Orgao antigo = new Orgao();
                antigo.setId(id);
                OrgaoDAO.desativar(antigo);

                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/desativarOrgao.jsp");
                disp.forward(req, resp);

            } else {
                RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/erro_bd.jsp");
                disp.forward(req, resp);
            }
        } catch (SQLException sqlerro) {
            RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/erro_bd.jsp");
            disp.forward(req, resp);
        } catch (NullPointerException nullerro) {
            RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/erro_bd.jsp");
            disp.forward(req, resp);
        } catch (JasperException jsperro) {
            RequestDispatcher disp = req.getRequestDispatcher("WEB-INF/pages/erro_bd.jsp");
            disp.forward(req, resp);
        }
    }
}
