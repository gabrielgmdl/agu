package br.gov.agu.servlets;

import br.gov.agu.dao.ConexaoDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ServletPatromonio")
public class ServletPatromonio extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter saida = resp.getWriter();
        String botao = req.getParameter("botao");
        //TESTE
        //NÃO IMPLEMENTADO AINDA
        if (botao.equals("conectar")) {
            try {
                Connection teste = new ConexaoDAO().pegarConexao();
                if (teste != null) {
                    saida.print("<!DOCTYPE html>\n"
                            + "<html>\n"
                            + "    <head>\n"
                            + "        <title>Teste</title>\n"
                            + "        <meta charset=\"UTF-8\">\n"
                            + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                            + "    </head>\n"
                            + "    <body>\n"
                            + "        <div>Funfou!</div>\n"
                            + "        <div>" + teste + "</div>\n"
                            + "    </body>\n"
                            + "</html>");
                } else {
                    {
                        saida.print("<!DOCTYPE html>\n"
                                + "<html>\n"
                                + "    <head>\n"
                                + "        <title>Teste</title>\n"
                                + "        <meta charset=\"UTF-8\">\n"
                                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                                + "    </head>\n"
                                + "    <body>\n"
                                + "        <div>Não Funfou!</div>\n"
                                + "        <div>" + teste + "</div>\n"
                                + "    </body>\n"
                                + "</html>");
                    }
                }
            } catch (SQLException erro) {

                saida.print("<!DOCTYPE html>\n"
                        + "<html>\n"
                        + "    <head>\n"
                        + "        <title>Teste</title>\n"
                        + "        <meta charset=\"UTF-8\">\n"
                        + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                        + "    </head>\n"
                        + "    <body>\n"
                        + "        <div>ERRO, NÃO Funfou!<br> +" + erro.getMessage() + "+</div>\n"
                        + "    </body>\n"
                        + "</html>");
            }
        }
    }
}
