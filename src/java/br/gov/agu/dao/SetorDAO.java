package br.gov.agu.dao;

import br.gov.agu.beans.Setor;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SetorDAO {
    
    public static void inserir(Setor novo) throws SQLException {
        String sql = "INSERT INTO `setor`(`nome`, `id_unidade`, `created`, `ativo`, `id`) VALUES (?, ?, now(), default ,default)";
        PreparedStatement pstm = new ConexaoDAO().pegarConexao().prepareStatement(sql);
        pstm.setObject(1, novo.getNome());
        pstm.setObject(2, novo.getId_unidade());        
        pstm.execute();
        pstm.close();
    }

    public static void editar(Setor antigo) throws SQLException {
        String sql = "UPDATE `setor`"
                + "SET `nome` = ? , `id_unidade` = ? , `modified` = now()"
                + "WHERE `id` = ?";
        PreparedStatement pstm = new ConexaoDAO().pegarConexao().prepareStatement(sql);
        pstm.setObject(1, antigo.getNome());
        pstm.setObject(2, antigo.getId_unidade());        
        pstm.setObject(4, antigo.getId());
        pstm.execute();
        pstm.close();
    }
    
    public static ArrayList<Setor> buscarTodos() throws SQLException {
        ArrayList<Setor> unidades = new ArrayList<>();
        String sql = "SELECT * FROM `setor` ORDER BY id ASC";
        PreparedStatement pstm = new ConexaoDAO().pegarConexao().prepareStatement(sql);
        ResultSet rs = pstm.executeQuery();
        while (rs.next()) {
            Setor setor = new Setor();
            setor.setNome(rs.getString("nome"));
            setor.setId_unidade(Integer.parseInt(rs.getString("id_unidade")));            
            setor.setCreated(rs.getString("created"));
            setor.setModified(rs.getString("modified"));
            setor.setAtivo(Boolean.getBoolean(rs.getString("ativo")));
            setor.setId(Integer.parseInt(rs.getString("id")));
            unidades.add(setor);
        }
        rs.close();
        return unidades;
    }
    
    public static void ativar(Setor setor) throws SQLException {
        String sql = "UPDATE `setor` SET `ativo` = 1, `modified` = now() WHERE `id` = ?";
        PreparedStatement pstm = new ConexaoDAO().pegarConexao().prepareStatement(sql);        
        pstm.setObject(1, setor.getId());
        pstm.execute();
        pstm.close();                
    }
    
    public static void desativar(Setor setor) throws SQLException {
        String sql = "UPDATE `setor` SET `ativo` = 0, `modified` = now() WHERE `id` = ?";
        PreparedStatement pstm = new ConexaoDAO().pegarConexao().prepareStatement(sql);
        pstm.setObject(1, setor.getId());
        pstm.execute();
        pstm.close();                
    }
    
}