package br.gov.agu.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexaoDAO {

    private String url = "jdbc:mysql://localhost/bd_geral_agu";
    private String user = "root";
    private String pass = "";

    public Connection pegarConexao() throws SQLException {
        Connection con = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, pass);
            System.out.println("Conexão realizada!");
        } catch (SQLException ex) {
            System.out.println("Problemas ao conectar ao Driver" + ex.getMessage());
        } catch (ClassNotFoundException erro) {
            System.out.println("Driver não encontrado" + erro.getMessage());
        }
        return con;
    }
}
