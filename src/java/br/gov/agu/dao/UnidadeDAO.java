package br.gov.agu.dao;

import br.gov.agu.beans.Unidade;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UnidadeDAO {
    public static void inserir(Unidade nova) throws SQLException {
        String sql = "INSERT INTO `unidade`(`nome`, `id_orgao`, `created`, `ativo`, `id`) VALUES (?, ?, now(), default ,default)";
        PreparedStatement pstm = new ConexaoDAO().pegarConexao().prepareStatement(sql);
        pstm.setObject(1, nova.getNome());
        pstm.setObject(2, nova.getId_orgao());        
        pstm.execute();
        pstm.close();
    }

    public static void editar(Unidade antiga) throws SQLException {
        String sql = "UPDATE `unidade`"
                + "SET `nome` = ? , `id_orgao` = ? , `modified` = now()"
                + "WHERE `id` = ?";
        PreparedStatement pstm = new ConexaoDAO().pegarConexao().prepareStatement(sql);
        pstm.setObject(1, antiga.getNome());
        pstm.setObject(2, antiga.getId_orgao());        
        pstm.setObject(4, antiga.getId());
        pstm.execute();
        pstm.close();
    }
    
    public static ArrayList<Unidade> buscarTodos() throws SQLException {
        ArrayList<Unidade> unidades = new ArrayList<>();
        String sql = "SELECT * FROM `unidade` ORDER BY id ASC";
        PreparedStatement pstm = new ConexaoDAO().pegarConexao().prepareStatement(sql);
        ResultSet rs = pstm.executeQuery();
        while (rs.next()) {
            Unidade unidade = new Unidade();
            unidade.setNome(rs.getString("nome"));
            unidade.setId_orgao(Integer.parseInt(rs.getString("id_orgao")));            
            unidade.setCreated(rs.getString("created"));
            unidade.setModified(rs.getString("modified"));
            unidade.setAtivo(Boolean.getBoolean(rs.getString("ativo")));
            unidade.setId(Integer.parseInt(rs.getString("id")));
            unidades.add(unidade);
        }
        rs.close();
        return unidades;
    }
    
    public static ArrayList<Unidade> buscarPorOrgao(int id) throws SQLException {
        ArrayList<Unidade> unidades = new ArrayList<>();
        String sql = "SELECT * FROM `unidade` WHERE id_orgao = ? ORDER BY id ASC";
        PreparedStatement pstm = new ConexaoDAO().pegarConexao().prepareStatement(sql);
        pstm.setObject(1, id);
        ResultSet rs = pstm.executeQuery();
        while (rs.next()) {
            Unidade unidade = new Unidade();
            unidade.setNome(rs.getString("nome"));
            unidade.setId_orgao(Integer.parseInt(rs.getString("id_orgao")));            
            unidade.setCreated(rs.getString("created"));
            unidade.setModified(rs.getString("modified"));
            unidade.setAtivo(Boolean.getBoolean(rs.getString("ativo")));
            unidade.setId(Integer.parseInt(rs.getString("id")));
            unidades.add(unidade);
        }
        rs.close();
        return unidades;
    }
    
    public static void ativar(Unidade unidade) throws SQLException {
        String sql = "UPDATE `unidade` SET `ativo` = 1, `modified` = now() WHERE `id` = ?";
        PreparedStatement pstm = new ConexaoDAO().pegarConexao().prepareStatement(sql);        
        pstm.setObject(1, unidade.getId());
        pstm.execute();
        pstm.close();                
    }
    
    public static void desativar(Unidade unidade) throws SQLException {
        String sql = "UPDATE `unidade` SET `ativo` = 0, `modified` = now() WHERE `id` = ?";
        PreparedStatement pstm = new ConexaoDAO().pegarConexao().prepareStatement(sql);
        pstm.setObject(1, unidade.getId());
        pstm.execute();
        pstm.close();                
    }
}