package br.gov.agu.dao;

import br.gov.agu.beans.Orgao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class OrgaoDAO {

    public static void inserir(Orgao novo) throws SQLException {
        String sql = "INSERT INTO `orgao`(`nome`, `estado`, `cidade`, `created`, `ativo`, `id`) VALUES (?,?,?,now(),default,default)";
        PreparedStatement pstm = new ConexaoDAO().pegarConexao().prepareStatement(sql);
        pstm.setObject(1, novo.getNome());
        pstm.setObject(2, novo.getEstado());
        pstm.setObject(3, novo.getCidade());
        pstm.execute();
        pstm.close();
    }

    public static void editar(Orgao antigo) throws SQLException {
        String sql = "UPDATE `orgao`"
                + "SET `nome` = ? , `estado` = ? , `cidade` = ? , `modified` = now()"
                + "WHERE `id` = ?";
        PreparedStatement pstm = new ConexaoDAO().pegarConexao().prepareStatement(sql);
        pstm.setObject(1, antigo.getNome());
        pstm.setObject(2, antigo.getEstado());
        pstm.setObject(3, antigo.getCidade());
        pstm.setObject(4, antigo.getId());
        pstm.execute();
        pstm.close();
    }
    
    public static ArrayList<Orgao> buscarTodos() throws SQLException {
        ArrayList<Orgao> orgaos = new ArrayList<>();
        String sql = "SELECT * FROM `orgao` ORDER BY id ASC";
        PreparedStatement pstm = new ConexaoDAO().pegarConexao().prepareStatement(sql);
        ResultSet rs = pstm.executeQuery();
        while (rs.next()) {
            Orgao orgao = new Orgao();
            orgao.setNome(rs.getString("nome"));
            orgao.setEstado(rs.getString("estado"));
            orgao.setCidade(rs.getString("cidade"));
            orgao.setCreated(rs.getString("created"));
            orgao.setModified(rs.getString("modified"));
            orgao.setAtivo(Boolean.getBoolean(rs.getString("ativo")));
            orgao.setId(Integer.parseInt(rs.getString("id")));
            orgaos.add(orgao);
        }
        rs.close();
        return orgaos;
    }
    
    public static void ativar(Orgao orgao) throws SQLException {
        String sql = "UPDATE `orgao` SET `ativo` = 1, `modified` = now() WHERE `id` = ?";
        PreparedStatement pstm = new ConexaoDAO().pegarConexao().prepareStatement(sql);        
        pstm.setObject(1, orgao.getId());
        pstm.execute();
        pstm.close();                
    }
    
    public static void desativar(Orgao orgao) throws SQLException {
        String sql = "UPDATE `orgao` SET `ativo` = 0, `modified` = now() WHERE `id` = ?";
        PreparedStatement pstm = new ConexaoDAO().pegarConexao().prepareStatement(sql);
        pstm.setObject(1, orgao.getId());
        pstm.execute();
        pstm.close();                
    }
}