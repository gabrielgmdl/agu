<%-- 
    Document   : acesso_restrito
    Created on : 15/07/2018, 01:46:37
    Author     : Gabriel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Acesso Restrito</title>
        <style type="text/css">
            body {
                margin-top: 10%;
                text-align: center;
                font-size: 25px;
            }            
        </style>
    </head>
    <body>
        <h1>Ooops! Parece que você não pode acessar esta página.</h1>
        <br>
        <h1>Efetue o login novamente ou entre em contato com o suporte técnico.</h1>
        <br>
    </body>
</html>
