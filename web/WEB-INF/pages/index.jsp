<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./estilo.css">
        <link rel="icon"  href="./imgs/agu.jpg">
        <meta charset="utf-8">
        <title>SOP-AGU</title>
    </head>
    <body>
        <header><h3>Advocacia-Geral da União</h3> <img src="./imgs/agu-header.png" class="img_header"></header>
        <div class="sop">SOP-AGU</div>
        <div class="menu-container">
            <ul id="nav">
                <div class="inicio"><a href="/AGU"><img src="./imgs/home.png" class="img_home" <a href="/AGU" class="ini">Inicio</a></a></div>
            </ul>
            <ul id="nav"> 
                <li><a>Patrimônio</a> 
                    <ul class="sub"> 
                        <form action="Patrimonio">
                            <input type="submit" value="Listar" />
                        
                            <input type="submit" value="Inventário" />
                        </form>
                    </ul> 
                </li>
            </ul>


            <ul id="nav"> 
                <li><a>Órgão</a> 
                    <ul class="sub"> 
                        <form method="POST" action="Orgao">
                            <input type="submit" name="botao" value="Cadastrar"/>
                        
                            <input type="submit" name="botao" value="Editar" />
                        
                            <input type="submit" name="botao" value="Ativar" />
                        
                            <input type="submit" name="botao" value="Desativar" />
                        </form>
                    </ul> 
                </li>
            </ul>

            <ul id="nav"> 
                <li><a>Unidade</a> 
                    <ul class="sub"> 
                        <form method="POST" action="Unidade">
                            <input type="submit" name="botao" value="Cadastrar"/>
                        
                            <input type="submit" name="botao" value="Editar"/>
                        
                            <input type="submit" name="botao" value="Ativar"/>
                        
                            <input type="submit" name="botao" value="Desativar"/>
                        </form>
                    </ul> 
                </li>
            </ul>

            <ul id="nav"> 
                <li><a>Setor</a> 
                    <ul class="sub"> 
                        <form method="POST" action="Setor">
                            <input type="submit" value="Cadastrar" />
                        
                            <input type="submit" value="Editar" />
                            
                            <input type="submit" value="Ativar" />
                            
                            <input type="submit" value="Desativar" />
                        </form>
                    </ul> 
                </li>
            </ul>

            <ul id="nav"> 
                <li><a>Sala</a> 
                    <ul class="sub"> 
                        <form method="POST" action="Sala">
                            <input type="submit" value="Cadastrar" />
                        
                            <input type="submit" value="Editar" />
                            
                            <input type="submit" value="Ativar" />
                            
                            <input type="submit" value="Desativar" />
                        </form>
                    </ul> 
                </li>
            </ul>

            <ul id="nav"> 
                <li><a>Funcionário</a> 
                    <ul class="sub"> 
                        <form method="POST" action="Funcionario">
                            <input type="submit" value="Cadastrar" />
                        
                            <input type="submit" value="Desativar" />
                        </form>
                    </ul> 
                </li>
            </ul>

            <ul id="nav"> 
                <li><a>Estoque</a> 
                    <ul class="sub"> 
                        <form method="POST" action="Estoque">
                            <input type="submit" value="Cadastrar" />
                        
                            <input type="submit" value="Ativar" />
                            
                            <input type="submit" value="Desativar" />
                        </form>
                    </ul> 
                </li>
            </ul>

            <ul id="nav"> 
                <li><a href="">Sair</a>  
                </li>
            </ul>
        </div>
        <div class="container_movimentacao">
            <form class="form"></form>
        </div>
        <footer><h4>Av. Afonso Pena, 6.134 - - Chácara Cachoeira - Campo Grande - MS - Cep. 79040-010 - (67) 33207300</h4> <img src="./imgs/agu_footer.png" class="img_footer"></footer>
    </body>
</html>