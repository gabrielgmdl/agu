<%@page import="java.sql.SQLException"%>
<%@page import="br.gov.agu.dao.OrgaoDAO"%>
<%@page import="br.gov.agu.beans.Orgao"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./estilo.css">
        <link rel="icon"  href="./imgs/agu.jpg">
        <script src="./jquery-3.3.1.js" type="text/javascript"></script>
        <script src="./dropdown_estado_cidade.js" type="text/javascript"></script>
        <meta charset="utf-8">
        <title>SOP-AGU</title>
    </head>
    <body>
        <header><h3>Advocacia-Geral da União</h3> <img src="./imgs/agu-header.png" class="img_header"></header>
        <div class="sop">SOP-AGU</div>
        <div class="menu-container">
            <ul id="nav">
                <div class="inicio"><a href="/AGU"><img src="./imgs/home.png" class="img_home" <a href="/AGU" class="ini">Inicio</a></a></div>
            </ul>
            <ul id="nav"> 
                <li><a>Patrimônio</a> 
                    <ul class="sub"> 
                        <form action="Patrimonio">
                            <input type="submit" value="Listar" />
                        
                            <input type="submit" value="Inventário" />
                        </form>
                    </ul> 
                </li>
            </ul>


            <ul id="nav"> 
                <li><a>Órgão</a> 
                    <ul class="sub"> 
                        <form method="POST" action="Orgao">
                            <input type="submit" name="botao" value="Cadastrar"/>
                        
                            <input type="submit" name="botao" value="Editar" />
                        
                            <input type="submit" name="botao" value="Ativar" />
                        
                            <input type="submit" name="botao" value="Desativar" />
                        </form>
                    </ul> 
                </li>
            </ul>

            <ul id="nav"> 
                <li><a>Unidade</a> 
                    <ul class="sub"> 
                        <form method="POST" action="Unidade">
                            <input type="submit" name="botao" value="Cadastrar"/>
                        
                            <input type="submit" name="botao" value="Editar"/>
                        
                            <input type="submit" name="botao" value="Ativar"/>
                        
                            <input type="submit" name="botao" value="Desativar"/>
                        </form>
                    </ul> 
                </li>
            </ul>

            <ul id="nav"> 
                <li><a>Setor</a> 
                    <ul class="sub"> 
                        <form method="POST" action="Setor">
                            <input type="submit" value="Cadastrar" />
                        
                            <input type="submit" value="Editar" />
                            
                            <input type="submit" value="Ativar" />
                            
                            <input type="submit" value="Desativar" />
                        </form>
                    </ul> 
                </li>
            </ul>

            <ul id="nav"> 
                <li><a>Sala</a> 
                    <ul class="sub"> 
                        <form method="POST" action="Sala">
                            <input type="submit" value="Cadastrar" />
                        
                            <input type="submit" value="Editar" />
                            
                            <input type="submit" value="Ativar" />
                            
                            <input type="submit" value="Desativar" />
                        </form>
                    </ul> 
                </li>
            </ul>

            <ul id="nav"> 
                <li><a>Funcionário</a> 
                    <ul class="sub"> 
                        <form method="POST" action="Funcionario">
                            <input type="submit" value="Cadastrar" />
                        
                            <input type="submit" value="Desativar" />
                        </form>
                    </ul> 
                </li>
            </ul>

            <ul id="nav"> 
                <li><a>Estoque</a> 
                    <ul class="sub"> 
                        <form method="POST" action="Estoque">
                            <input type="submit" value="Cadastrar" />
                        
                            <input type="submit" value="Ativar" />
                            
                            <input type="submit" value="Desativar" />
                        </form>
                    </ul> 
                </li>
            </ul>

            <ul id="nav"> 
                <li><a href="">Sair</a>  
                </li>
            </ul>
        </div>
        <h2 class="editar_h2">Editar Órgãos</h2>
        <form class="form_editar" method="POST" action="Orgao">
              <div class="container">
                <h2>EDITAR</h2>        
                <a class="nome_orgao">Nome:</a>
                <select name="id" id="id" required>
                    <option value="">Escolha um órgão</option>
                    <%
                        ArrayList<Orgao> orgaos = new ArrayList<>();
                        try {
                            orgaos = OrgaoDAO.buscarTodos();
                            for (Orgao orgao : orgaos) { %>
                    <option value="<% out.print(orgao.getId());%>"><% out.print(orgao.getNome());%></option>
                    <%}
                                    } catch (SQLException erro) {%>
                    <option value="<%=erro.getMessage()%>"><%=erro.getMessage()%></option>
                    <% }%>                                        
                </select><br>
                <a class="novo_nome">Novo Nome:</a>
                <input type="text" name="orgao" placeholder="NOVO NOME"/><br><br>
                <a class="estado">Estado:</a>
                <select name="estado" id="estado">
                  <option value=""></option>
                </select>
                <a class="cidade">Cidade:</a>
                <select name="cidade" id="cidade"></select>
                <a class="senha">Senha:</a>
                <input type="password" name="senha"placeholder="SENHA">
                <button class="botao_orgao" name="botao" value="editar">EDITAR</button>
              </div>
        </form>
        <footer><h4>Av. Afonso Pena, 6.134 - - Chácara Cachoeira - Campo Grande - MS - Cep. 79040-010 - (67) 33207300</h4> <img src="./imgs/agu_footer.png" class="img_footer"></footer>
    </body>
</html>