<%-- 
    Document   : erro_bd
    Created on : 15/07/2018, 01:42:45
    Author     : Gabriel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Erro DB</title>
        <style type="text/css">
            body {
                margin-top: 10%;
                text-align: center;
                font-size: 25px;
            }            
        </style>
    </head>
    <body>
        <h1>Sistema temporariamente em manutenção.</h1>
        <br>
        <h1>Tente novamente mais tarde!</br>
        <br>        
    </body>
</html>
