$(document).ready(function () {

    $.getJSON('estado_cidade.json', function (data) {
        var items = [];
        var options = '<option value="">Escolha uma UF</option>';
        $.each(data, function (key, val) {
            options += '<option value="' + val.nome + '">' + val.nome + '</option>';
        });
        $("#estado").html(options);

        $("#estado").change(function () {

            var options_cidade = '';
            var str = "";

            $("#estado option:selected").each(function () {
                str += $(this).text();
            });

            $.each(data, function (key, val) {
                if (val.nome == str) {
                    $.each(val.cidade, function (key_city, val_city) {
                        options_cidade += '<option value="' + val_city + '">' + val_city + '</option>';
                    });
                }
            });
            $("#cidade").html(options_cidade);

        }).change();

    });

});